import numpy as np
import re
import string
import configparser
import csv
import pandas as pd
import copy

from num2words import num2words
from numbertoletters import number_to_letters
from nltk import word_tokenize
from roman import fromRoman


month_dict = {1 : 'enero', 2 : 'febrero', 3 : 'marzo', 4 : 'abril', 5 : 'mayo', 6 : 'junio', 7 : 'julio',
              8 : 'agosto', 9 : 'septiembre', 10 : 'octubre', 11 : 'noviembre', 12 : 'diciembre'}

def remove_tag(str):
    cls = {'<PUNCT>': '</PUNCT>',
           '<MEASURE>': '</MEASURE>',
           '<CARDINAL>': '</CARDINAL>',
           '<DATE>': '</DATE>',
           '<TIME>': '</TIME>',
           '<VERBATIM>': '</VERBATIM>',
           '<ROMAN>': '</ROMAN>',
           '<DECIMAL>': '</DECIMAL>',
           '<ADDRESS>': '</ADDRESS>',
           '<FRACTION>': '</FRACTION>',
           '<ABBRE>': '</ABBRE>',
           '<FOREIGN>': '</FOREIGN>',
           '<DIGIT>': '</DIGIT>',
           '<LETTER>': '</LETTER>'}
    for tag_start, tag_end in cls.items():
        str = str.replace(tag_start, '')
        str = str.replace(tag_end, '')

    return str


def normalize_datetime(input_str, output_str):
    """
    Normalize datetime such as 12/12/2012 or 4/5/96'
    currently cannot differentiate sport score so only normalize 3 triplets 
    """
#     month_dict = {1 : 'uno', 2 : 'dos', 3 : 'tres', 4 : 'cuatro', 5 : 'cinco', 6 : 'seis', 7 : 'siete',
#                   8 : 'ocho', 9 : 'nueve', 10 : 'diez', 11 : 'once', 12 : 'doce', 13: 'trece', 14: 'catorce',
#                   15: 'quince', 16:'dieciséis', 17:'diecisiete', 18:'dieciocho', 19:'diecinueve',
#                   20: 'veinte', 21:'veintiuno', 22:'veintidós', 23:'veintitrés', 24:'veinticuatro',
#                   25: 'veinticinco', 26:'veintiséis',27:'veintisiete', 28:'veintiocho', 29:'veintinueve'}
    
    
    input_str = ' ' + input_str + ' '
    output_str = ' ' + output_str + ' '
    type_1 = re.findall(r"(\s|[E]l dia\s)?([0-9]{1,2})[-/:]([0-9]{1,2})[-/:]([0-9]{2,4})+(\s|$)",input_str)
    
  
    if len(type_1) > 2:
        for item in type_1:

            ngay, day, month, year = item[0], item[1], item[2], item[3]
            
            string_0 = ngay+day+'/'+month+'/'+year
            string_1 = ngay+day+':'+month+':'+year
            string_2 = ngay+day+'-'+month+'-'+year
            
            if int(month) > 12:
                day, month = month, day
            
#             print(month)
            if (int(month) == 0) or (int(day) > 12):
                replace_str = time2words(item[1]+':'+item[2]+':'+item[3])
                output_str = output_str.replace(string_0, replace_str, 1)
                output_str = output_str.replace(string_1, replace_str, 1)
                output_str = output_str.replace(string_2, replace_str, 1)
            else:
                day = ngay + day
                month = 'de ' + month_dict[int(month)]
                year = 'de ' + year

                replaced = day + ' ' + month + ' ' + year

                input_str = input_str.replace(string_0, replaced, 1)
                input_str = input_str.replace(string_1, replaced, 1)
                input_str = input_str.replace(string_2, replaced, 1)

                output_str = output_str.replace(string_0, replaced, 1)
                output_str = output_str.replace(string_1, replaced, 1)
                output_str = output_str.replace(string_2, replaced, 1) 
            
    type_2 = re.findall(r"(\s|[E]l dia\s)?([0-9]{1,2})[//]([0-9]{1,2})+(\s|$)", input_str)
    if len(type_2) > 1:
        for item in type_2:
            
            ngay, day, month = item[0], item[1], item[2]
            
            string_0 = ngay+day+'/'+month
            string_1 = ngay+day+':'+month
            string_2 = ngay+day+'-'+month
            
            if int(month) > 12:
                day, month = month, day
            
            if (int(month) == 0) or (int(day) > 12):
                replace_str = time2words(item[1]+':'+item[2])
                output_str = output_str.replace(string_0, replace_str, 1)
                output_str = output_str.replace(string_1, replace_str, 1)
                output_str = output_str.replace(string_2, replace_str, 1)
            else:
                if len(day) == 1:
                    day = '0' + day
                if len(month) == 1:
                    month = '0' + month

                day = ngay + day
                month = 'de ' + month_dict[int(month)]

                replaced = day + ' ' + month

                input_str = input_str.replace(string_0, replaced, 1)
                input_str = input_str.replace(string_1, replaced, 1)
                input_str = input_str.replace(string_2, replaced, 1)

                output_str = output_str.replace(string_0, replaced, 1)
                output_str = output_str.replace(string_1, replaced, 1)
                output_str = output_str.replace(string_2, replaced, 1) 

    input_str = re.sub(r'\b(el dia)( \1\b)+', r'\1', input_str) #remove duplicated el dia in row
    output_str = re.sub(r'\b(el dia)( \1\b)+', r'\1', output_str) #remove duplicated el dia in row
    
    return input_str, output_str


def normalize_dottedwords(input_str, output_str):
    """
    Normalize sequences with forms a.b or a.b.c
    Currently under improvement
    """
    input_str = ' ' + input_str + ' '
    output_str = ' ' + output_str + ' '
    type_1 = re.findall('\s(\w+)\.(\w+)\.(?:(\w+))+(?=[\s]|$)', input_str)
    if len(type_1) > 0:
        for item in type_1:
            words = ''.join(item)
            replaced = '.'.join(item)
            input_str = input_str.replace(replaced, words)
            output_str = output_str.replace(replaced, words)
    
    type_2 = re.findall('\s(\w+)\.(\w+)+(?=[\s]|$)', input_str)
    if len(type_2) > 0:
        for item in type_2:
            words = ''.join(item)
            replaced = '.'.join(item)
            input_str = input_str.replace(replaced, words)
            output_str = output_str.replace(replaced, words)
            
    return input_str, output_str


import re
from num2words import num2words
import string

def unit2words(input_str, output_str):
    # Units of information
    # input_str = input_str.replace('KB ', ' <MEASURE>KB</MEASURE> ')
    # input_str = input_str.replace('MB ', ' <MEASURE>MB</MEASURE> ')
    # input_str = input_str.replace('GB ', ' <MEASURE>GB</MEASURE> ')
    # input_str = input_str.replace('TB ', ' <MEASURE>TB</MEASURE> ')

    # 2G, 3G, etc
    input_str = input_str.replace(' 2G ', ' <MEASURE>2G</MEASURE> ')
    input_str = input_str.replace(' 3G ', ' <MEASURE>3G</MEASURE> ')
    input_str = input_str.replace(' 4G ', ' <MEASURE>4G</MEASURE> ')
    input_str = input_str.replace(' 5G ', ' <MEASURE>5G</MEASURE> ')

    # Units of frequency
    input_str = input_str.replace('GHz', ' <MEASURE>GHz</MEASURE> ')
    input_str = input_str.replace('MHz', ' <MEASURE>MHz</MEASURE> ')

    # Units of data-rate
    input_str = input_str.replace('Mbps', ' <MEASURE>Mbps</MEASURE> ')
    input_str = input_str.replace('Mb/s', ' <MEASURE>Mb/s</MEASURE> ')

    # Units of currency
    input_str = input_str.replace("USD/", " <MEASURE>USD/</MEASURE> ")
    input_str = input_str.replace('e ', ' <MEASURE>e</MEASURE> ')
    input_str = input_str.replace('$', ' <MEASURE>$</MEASURE> ')
    input_str = input_str.replace('USD', ' <MEASURE>USD</MEASURE> ')
    input_str = input_str.replace('usd', ' <MEASURE>USD</MEASURE> ')
    input_str = input_str.replace('eur', ' <MEASURE>eur</MEASURE> ')
    input_str = input_str.replace('EUR', ' <MEASURE>EUR</MEASURE> ') 
    input_str = input_str.replace('vnd', ' <MEASURE>vnd</MEASURE> ')
    input_str = input_str.replace('VND', ' <MEASURE>VND</MEASURE> ')

    # Units of area
    input_str = input_str.replace('km2', ' <MEASURE>km2</MEASURE> ')
    input_str = input_str.replace('cm2', ' <MEASURE>cm2</MEASURE> ')
    input_str = input_str.replace('mm2', ' <MEASURE>mm2</MEASURE> ')
    input_str = input_str.replace('m2', ' <MEASURE>m2</MEASURE> ')
    input_str = input_str.replace(' ha ', ' <MEASURE>ha</MEASURE> ')

    # Units of length
    # input_str = input_str.replace(' km ', ' <MEASURE>km</MEASURE> ')
    # input_str = input_str.replace(' cm ', ' <MEASURE>cm</MEASURE> ')
    # input_str = input_str.replace(' mm ', ' <MEASURE>mm</MEASURE> ')
    # input_str = input_str.replace(' nm ', ' <MEASURE>nm</MEASURE> ')
    input_str = input_str.replace('inch ', ' <MEASURE>inch</MEASURE> ')

    # Units of volume
    # input_str = input_str.replace('ml ', ' <MEASURE>ml</MEASURE> ')
    input_str = input_str.replace('cm3 ', ' <MEASURE>cm3</MEASURE> ')
    # input_str = input_str.replace('cc ', ' <MEASURE>cc</MEASURE> ')
    input_str = input_str.replace('m3 ', ' <MEASURE>m3</MEASURE> ')

    # Units of weight
    input_str = input_str.replace('/kg', ' <MEASURE>/kg</MEASURE> ')
    input_str = input_str.replace('kg/', ' <MEASURE>kg/</MEASURE> ')
    # input_str = input_str.replace('kg ', ' <MEASURE>kg</MEASURE> ')
    input_str = input_str.replace(' grams ', ' <MEASURE>grams</MEASURE> ')
    # input_str = input_str.replace(' mg ', ' <MEASURE>mg</MEASURE> ')

    # Units of temperature
    input_str = input_str.replace("oC ", " <MEASURE>oC</MEASURE> ")
    input_str = input_str.replace("ºC ", " <MEASURE>ºC</MEASURE> ")
    input_str = input_str.replace("ºF ", " <MEASURE>ºF</MEASURE> ")

    # Picture element
    # input_str = input_str.replace('MP ', ' <MEASURE>MP</MEASURE> ')

    # Units of speed
    input_str = input_str.replace("bpm", " <MEASURE>bpm</MEASURE> ")
    input_str = input_str.replace("nm/s", " <MEASURE>nm/s</MEASURE> ")
    input_str = input_str.replace("µm/s", " <MEASURE>µm/s</MEASURE> ")
    input_str = input_str.replace("mm/s", " <MEASURE>mm/s</MEASURE> ")
    input_str = input_str.replace("cm/s", " <MEASURE>cm/s</MEASURE> ")
    input_str = input_str.replace("dm/s", " <MEASURE>dm/s</MEASURE> ")
    input_str = input_str.replace("dam/s", " <MEASURE>dam/s</MEASURE> ")
    input_str = input_str.replace("hm/s", " <MEASURE>hm/s</MEASURE> ")
    input_str = input_str.replace("km/s", " <MEASURE>km/s</MEASURE> ")
    input_str = input_str.replace("m/s", " <MEASURE>m/s</MEASURE> ")
    input_str = input_str.replace("nm/segundo", " <MEASURE>nm/segundo</MEASURE> ")
    input_str = input_str.replace("µm/segundo", " <MEASURE>µm/segundo</MEASURE> ")
    input_str = input_str.replace("mm/segundo", " <MEASURE>mm/segundo</MEASURE> ")
    input_str = input_str.replace("cm/segundo", " <MEASURE>cm/segundo</MEASURE> ")
    input_str = input_str.replace("dm/segundo", " <MEASURE>dm/segundo</MEASURE> ")
    input_str = input_str.replace("dam/segundo", " <MEASURE>dam/segundo</MEASURE> ")
    input_str = input_str.replace("hm/segundo", " <MEASURE>hm/segundo</MEASURE> ")
    input_str = input_str.replace("km/segundo", " <MEASURE>km/segundo</MEASURE> ")
    input_str = input_str.replace("m/segundo", " <MEASURE>m/segundo</MEASURE> ")
    input_str = input_str.replace("nm/h", " <MEASURE>nm/h</MEASURE> ")
    input_str = input_str.replace("µm/h", " <MEASURE>µm/h</MEASURE> ")
    input_str = input_str.replace("mm/h", " <MEASURE>mm/h</MEASURE> ")
    input_str = input_str.replace("cm/h", " <MEASURE>cm/h</MEASURE> ")
    input_str = input_str.replace("dm/h", " <MEASURE>dm/h</MEASURE> ")
    input_str = input_str.replace("dam/h", " <MEASURE>dam/h</MEASURE> ")
    input_str = input_str.replace("hm/h", " <MEASURE>hm/h</MEASURE> ")
    input_str = input_str.replace("km/h", " <MEASURE>km/h</MEASURE> ")
    input_str = input_str.replace("kmh", " <MEASURE>kmh</MEASURE> ")
    input_str = input_str.replace("m/h", " <MEASURE>m/h</MEASURE> ")
    input_str = input_str.replace("nm/hora", " <MEASURE>nm/hora</MEASURE> ")
    input_str = input_str.replace("µm/hora", " <MEASURE>µm/hora</MEASURE> ")
    input_str = input_str.replace("mm/hora", " <MEASURE>mm/hora</MEASURE> ")
    input_str = input_str.replace("cm/hora", " <MEASURE>cm/hora</MEASURE> ")
    input_str = input_str.replace("dm/hora", " <MEASURE>dm/hora</MEASURE> ")
    input_str = input_str.replace("dam/hora", " <MEASURE>dam/hora</MEASURE> ")
    input_str = input_str.replace("hm/hora", " <MEASURE>hm/hora</MEASURE> ")
    input_str = input_str.replace("km/hora", " <MEASURE>km/hora</MEASURE> ")
    input_str = input_str.replace("m/hora", " <MEASURE>m/hora</MEASURE> ")

    # Others
    input_str = input_str.replace("/toneladas", " <MEASURE>/toneladas</MEASURE> ")
    input_str = input_str.replace("/trozo", " <MEASURE>/trozo</MEASURE> ")
    input_str = input_str.replace("/cada", " <MEASURE>/cada</MEASURE> ")
    input_str = input_str.replace("/años", " <MEASURE>/años</MEASURE> ")
    input_str = input_str.replace("/meses", " <MEASURE>/meses</MEASURE> ")
    input_str = input_str.replace("/dias", " <MEASURE>/dias</MEASURE> ")
    input_str = input_str.replace("/horas", " <MEASURE>/horas</MEASURE> ")
    input_str = input_str.replace("/minutos", " <MEASURE>/minutos</MEASURE> ")
    input_str = input_str.replace("e/litros", " <MEASURE>e/litros</MEASURE> ")
    input_str = input_str.replace("e/turno", " <MEASURE>e/turno</MEASURE> ")
    input_str = input_str.replace("persona/", " <MEASURE>persona/</MEASURE> ")
    input_str = input_str.replace("hora/", " <MEASURE>hora/</MEASURE> ")
    input_str = input_str.replace('%', ' <MEASURE>%</MEASURE> ')
    input_str = input_str.replace('mAh ', ' <MEASURE>mAh</MEASURE> ')
    input_str = input_str.replace(" lít/", " <MEASURE>lít/</MEASURE> ")
    input_str = input_str.replace("./", "")
    input_str = input_str.replace("Nm", " <MEASURE>Nm</MEASURE> ")
    input_str = input_str.replace("º", " <MEASURE>º</MEASURE> ")
    input_str = input_str.replace("mmol/l", " <MEASURE>mmol/l</MEASURE> ")
    input_str = input_str.replace("mg/", " <MEASURE>mg/</MEASURE> ")
    input_str = input_str.replace("g/km", " <MEASURE>g/km</MEASURE> ")
    input_str = input_str.replace("ounce", " <MEASURE>ounce</MEASURE> ")
    input_str = input_str.replace("m3/s", " <MEASURE>m3/s</MEASURE> ")

    # Units of information
    # output_str = output_str.replace('KB ', ' <MEASURE>ki lô bai</MEASURE> ')
    output_str = output_str.replace('Mb ', ' <MEASURE>megabyte</MEASURE> ')
    output_str = output_str.replace('Gb ', ' <MEASURE>Gigabyte</MEASURE> ')
    output_str = output_str.replace('GB ', ' <MEASURE>Gigabyte</MEASURE> ')
    output_str = output_str.replace('TB ', ' <MEASURE>Terabyte</MEASURE> ')
    
    # 2G, 3G, etc
    output_str = output_str.replace(' 2G ', ' <MEASURE>dos ge</MEASURE> ')
    output_str = output_str.replace(' 3G ', ' <MEASURE>tres ge</MEASURE> ')
    output_str = output_str.replace(' 4G ', ' <MEASURE>cuatro ge</MEASURE> ')
    output_str = output_str.replace(' 5G ', ' <MEASURE>cinco ge</MEASURE> ')
    
    # Units of frequency
    output_str = output_str.replace('GHz', ' <MEASURE>Giga Hertz</MEASURE> ')
    output_str = output_str.replace('MHz', ' <MEASURE></MEASURE> ')
    
    output_str = re.sub(r'\b(Mega Hertz)( \1\b)+', r'\1', output_str) #remove duplicated Mega Hertz in row

    # Units of data-rate
    output_str = output_str.replace('Mbps', ' <MEASURE>mega bit por segundo</MEASURE> ')
    output_str = output_str.replace('Mb/s', ' <MEASURE>mega bit por segundo</MEASURE> ')
    
    # Units of currency
    output_str = output_str.replace("e/", " <MEASURE>eur por</MEASURE> ")
    output_str = output_str.replace("USD/", " <MEASURE>USD por</MEASURE> ")
    output_str = output_str.replace('$', ' <MEASURE>USD por</MEASURE> ')
    output_str = output_str.replace('USD', ' <MEASURE>USD</MEASURE> ')
    output_str = output_str.replace('EUR', ' <MEASURE>EUR</MEASURE> ')
    output_str = output_str.replace('eur', ' <MEASURE>eur</MEASURE> ')

    # Units of area
    output_str = output_str.replace('km2', ' <MEASURE>kilómetros cuadrados</MEASURE> ')
    output_str = output_str.replace('cm2', ' <MEASURE>centímetros cuadrados</MEASURE> ')
    output_str = output_str.replace('mm2', ' <MEASURE>milímetros cuadrados</MEASURE> ')
    output_str = output_str.replace('m2', ' <MEASURE>metros cuadrados</MEASURE> ')
    
    # Units of length
    # output_str = output_str.replace(' km ', ' <MEASURE>kilómetros</MEASURE> ')
    # output_str = output_str.replace(' cm ', ' <MEASURE>centímetros</MEASURE> ')
    output_str = output_str.replace(' mm ', ' <MEASURE>milímetro</MEASURE> ')
    output_str = output_str.replace(' nm ', ' <MEASURE>nanometros</MEASURE> ')
    
    # Units of volume
    # output_str = output_str.replace('ml ', ' <MEASURE>milímetros cúbicos</MEASURE> ')
    output_str = output_str.replace('cm3 ', ' <MEASURE>centímetros cúbicos</MEASURE> ')
    # output_str = output_str.replace('cc ', ' <MEASURE>centímetros cúbicos</MEASURE> ')
    output_str = output_str.replace('m3 ', ' <MEASURE>metros cúbicos</MEASURE> ')

    # Units of weight
    output_str = output_str.replace('/kg', ' <MEASURE>por kilogramo</MEASURE> ')
    output_str = output_str.replace('kg/', ' <MEASURE>kilogramos por</MEASURE> ')
    # output_str = output_str.replace('kg ', ' <MEASURE>ki lô gam</MEASURE> ')
    output_str = output_str.replace(' grams ', ' <MEASURE>gramos</MEASURE> ')
    # output_str = output_str.replace(' mg ', ' <MEASURE>mi li gam</MEASURE> ')

    

    # Units of temperature
    output_str = output_str.replace("oC ", " <MEASURE>grado Ce</MEASURE> ")
    output_str = output_str.replace("ºC ", " <MEASURE>grado Ce</MEASURE> ")
    output_str = output_str.replace("ºF ", " <MEASURE>grado eFe</MEASURE> ")
    
    # Picture element
    # output_str = output_str.replace('MP ', ' <MEASURE>mê ga píc xeo</MEASURE> ')
    
    # Units of speed
    output_str = output_str.replace("bpm", " <MEASURE>latidos por minuto</MEASURE> ")
    output_str = output_str.replace("nm/s", " <MEASURE>nanometros por minuto</MEASURE> ")
    output_str = output_str.replace("µm/s", " <MEASURE>micrometros por minuto</MEASURE> ")
    output_str = output_str.replace("mm/s", " <MEASURE>milímetros por minuto</MEASURE> ")
    output_str = output_str.replace("cm/s", " <MEASURE>centímetros por minuto</MEASURE> ")
    output_str = output_str.replace("dm/s", " <MEASURE>decímetros por minuto</MEASURE> ")
    output_str = output_str.replace("hm/s", " <MEASURE>hectómetros por minuto</MEASURE> ")
    output_str = output_str.replace("km/s", " <MEASURE>kilómetros por minuto</MEASURE> ")
    output_str = output_str.replace("m/s", " <MEASURE>metros por minuto</MEASURE> ")
    output_str = output_str.replace("nm/h", " <MEASURE>nanometros por hora</MEASURE> ")
    output_str = output_str.replace("µm/h", " <MEASURE>micrometros por hora</MEASURE> ")
    output_str = output_str.replace("mm/h", " <MEASURE>milímetros por hora</MEASURE> ")
    output_str = output_str.replace("cm/h", " <MEASURE>centímetros por hora</MEASURE> ")
    output_str = output_str.replace("dm/h", " <MEASURE>decímetros por hora</MEASURE> ")
    output_str = output_str.replace("hm/h", " <MEASURE>hectómetros por hora</MEASURE> ")
    output_str = output_str.replace("km/h", " <MEASURE>kilómetros por hora</MEASURE> ")
    output_str = output_str.replace("kmh", " <MEASURE>kilómetros por hora</MEASURE> ")
    output_str = output_str.replace("m/h", " <MEASURE>metros por hora</MEASURE> ")


    # Others
    output_str = output_str.replace("/tonelada", " <MEASURE>por tonelada</MEASURE> ")
    output_str = output_str.replace("/barril", " <MEASURE>por barril</MEASURE> ")
    output_str = output_str.replace("/cada", " <MEASURE>por cada</MEASURE> ")
    output_str = output_str.replace("/año", " <MEASURE>por año</MEASURE> ")
    output_str = output_str.replace("/mes", " <MEASURE>por mes</MEASURE> ")
    output_str = output_str.replace("/día", " <MEASURE>por día</MEASURE> ")
    output_str = output_str.replace("/hora", " <MEASURE>por hora</MEASURE> ")
    output_str = output_str.replace("/minuto", " <MEASURE>por minuto</MEASURE> ")
    output_str = output_str.replace("e/litro", " <MEASURE>euro por litro</MEASURE> ")
    output_str = output_str.replace("e/turno", " <MEASURE>euro por turno</MEASURE> ")
    output_str = output_str.replace("personas/", " <MEASURE>personas por</MEASURE> ")
    output_str = output_str.replace("horas/", " <MEASURE>horas por</MEASURE> ")
    output_str = output_str.replace('%', ' <MEASURE>por ciento</MEASURE> ')
    output_str = output_str.replace('mAh ', ' <MEASURE>miliamperio</MEASURE> ')
    output_str = output_str.replace(" litros/", " <MEASURE>litros por</MEASURE> ")
    output_str = output_str.replace("./", "")
    output_str = output_str.replace("º", " <MEASURE>grado</MEASURE> ")
    output_str = output_str.replace("mmol/l", " <MEASURE>milimol por litro</MEASURE> ")
    output_str = output_str.replace("mg/", " <MEASURE>miligramos por</MEASURE> ")
    output_str = output_str.replace("millón/", " <MEASURE>millón por</MEASURE> ")
    output_str = output_str.replace("g/km", " <MEASURE>gramos por kilómetro</MEASURE> ")
    output_str = output_str.replace("oz", " <MEASURE>onza</MEASURE> ")
    output_str = output_str.replace("m3/s", " <MEASURE>metros cúbicos por minuto</MEASURE> ")
    
    return input_str, output_str

def money2words(input_str):
    number = input_str.split('k')[0]

    return num2words_fixed(number) +  ' mil'

def version2words(input_str):
    # Androi 2.2, 4.2.1...
    
    return input_str.replace('.', ' punto ')
    
def multiply(input_str):

    return input_str.replace('x', ' multiplicar ')


def num2words_float(input_str):
    # Fix num2words for reading spanish float numbers
    l_part = input_str.split(',')[0]
    r_part = input_str.split(',')[1]
    l_part_str = num2words_fixed(l_part)
    
    if len(r_part) < 3:
        r_part_str = num2words_fixed(r_part)
        return l_part_str + ' comma ' + r_part_str
    else:
        r_part_str = ''
        for num in r_part:
            r_part_str += num2words(int(num), lang='es') + ' '
        r_part_str.rstrip()

    return l_part_str + ' comma ' + r_part_str

def num2words_fixed(input_str):
    '''
    Keeping this does not change Spanish result
    Keeping this does not change Spanish result
    Keeping this does not change Spanish result
    Keeping this does not change Spanish result
    Keeping this does not change Spanish result
    Keeping this does not change Spanish result
    '''

    # Fix num2words for reading vietnamese numbers
    input_str = input_str.translate(str.maketrans('', '', string.punctuation))
    num2words_ = num2words(int(input_str), lang='es')
    
    # Cases: 205-'hai tram le nam' --> 'hai tram linh nam'
    if 'trăm lẻ' in num2words_:
        num2words_ = num2words_.replace('trăm lẻ', 'trăm linh')
        # Cases: 2005-'hai nghin le nam' --> 'hai nghin khong tram linh nam'
    
    special_terms = ['lẻ một', 'lẻ hai', 'lẻ ba', 'lẻ bốn', 'lẻ năm', 'lẻ sáu', 'lẻ bảy','lẻ tám','lẻ chín']
    for term in special_terms:
        if num2words_.endswith(term):
            num2words_ = num2words_.replace('lẻ', 'không trăm linh')
            break
    
    # Cases: 2035-'hai nghin le ba muoi lam' --> 'hai nghin khong tram ba muoi lam'
    if 'lẻ' in num2words_:
        num2words_ = num2words_.replace('lẻ', 'không trăm')
    
    return num2words_


def date_dmy2words(input_str):
    day, month, year = re.findall(r'[\d]+', input_str)
    day_words = num2words_fixed(day)
    month_words = num2words_fixed(month)
    
    if len(year) == 4:
        year_words = num2words(int(year), lang='es')
    elif len(year) == 2:
        year_words = num2words(int('20'+year), lang='es')
   
    year_words = num2words_fixed(year)
    output_str = day_words + ' de ' + month_words + ' de ' + year_words
    
    return output_str

def date_dm2words(input_str):
    day, month = re.findall(r'[\d]+', input_str)
    day_words = num2words_fixed(day)
    month_words = num2words_fixed(month)
    output_str = day_words + ' de ' + month_words
    
    return output_str

def date_my2words(input_str):
    month, year = re.findall(r'[\d]+', input_str)
    month_words = num2words_fixed(month)
    year_words = num2words_fixed(year)
    return month_words + ' de ' + year_words

def phone2words(phone_number):
    phone_digits = re.findall(r'[0-9]', phone_number)
    for index, phone_digit in enumerate(phone_digits):
        phone_digit_str = num2words(int(phone_digit), lang='es')
        phone_digits[index] = phone_digit_str

    phone_number_str = ' '.join(phone_digits)
    
    return phone_number_str

def time2words(time):
    if (time.find(':',3,6) != -1):
        time_hour = time.split(':')[0] 
        time_minute = time.split(':')[1]
        time_second = time.split(':')[2]
        time_hour_str = num2words(int(time_hour), lang='es')
        time_minute_str = num2words(int(time_minute), lang='es')
        time_second_str = num2words(int(time_second), lang='es')
        time_str = time_hour_str + time_minute_str + time_second_str
    elif (time.find('h') != -1):
        time_hour = time.split('h')[0] 
        time_minute = time.split('h')[1]
        time_hour_str = num2words(int(time_hour), lang='es')
        if time_minute == '' or time_minute == '00':
            return time_hour_str
        else:
            time_minute_str = num2words(int(time_minute), lang='es')
            time_str = time_hour_str + time_minute_str    
    elif (time.find(':') != -1):
        time_hour = time.split(':')[0] 
        time_minute = time.split(':')[1]
        time_hour_str = num2words(int(time_hour), lang='es')
        if time_minute == '' or time_minute == '00':
            return time_hour_str
        else:
            time_minute_str = num2words(int(time_minute), lang='es')
            time_str = time_hour_str + time_minute_str

    return time_str





