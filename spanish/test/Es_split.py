import re
import requests
import pandas as pd
import sys
import re
import os
import pandas as pd
from nltk.tokenize import sent_tokenize
from sentence_splitter import SentenceSplitter
from nltk import word_tokenize
from utils import *
from number import *


def clean_text(s):
    s=str(s)
#Thay thế kí tự có dấu và dấu tiếng Việt thành kí tự tương ứng trong tiếng Anh
    # s = re.sub(r'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
    # s = re.sub(r'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
    # s = re.sub(r'[èéẹẻẽêềếệểễ]', 'e', s)
    # s = re.sub(r'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
    # s = re.sub(r'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
    # s = re.sub(r'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
    # s = re.sub(r'[ìíịỉĩ]', 'i', s)
    # s = re.sub(r'[ÌÍỊỈĨ]', 'I', s)
    # s = re.sub(r'[ùúụủũưừứựửữ]', 'u', s)
    # s = re.sub(r'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
    # s = re.sub(r'[ỳýỵỷỹ]', 'y', s)
    # s = re.sub(r'[ỲÝỴỶỸ]', 'Y', s)
    # s = re.sub(r'[Đ]', 'D', s)
    # s = re.sub(r'[đ]', 'd', s)

#Remove some characters
    s = re.sub("\n", " . ", s)   
    s = re.sub("—", " . ", s)   
    # text = re.sub(r'[A,a]\/[C,c]', 'AC', text)
    # s = re.sub(r'[\«\»\–\“\”\‘\„\{\}\[\]\(\)\"\;]'," ", s) #Các dấu này norm bị lỗi phông hoặc không cần thiết
    # s = re.sub("  ", " ", s)
    
    return s

def split_para(dataframe):
    doc = {}
    df = dataframe.copy() #tránh việc các lệnh bên dưới làm thay đổi dataframe gốc 
    splitter = SentenceSplitter(language='es')
    punc_list = ["!",".","?"] #List of punctuation

    for id, row in df.iterrows():
        old_text=row["para"]
        new_text=clean_text(old_text)
        row.para=new_text
        sent_list = splitter.split(text=row.para)
        num_sent = len(sent_list)

        if num_sent == 1:
            print(id, sent_list)
            doc["{}_01".format(id)] = row[0]  #lấy original input ở cột thứ 2
        elif num_sent > 1:
            sent_index = 1
            for i in range(num_sent):
                if sent_list[i] not in punc_list: #Not consider " . ", " !", "?" as 1 sentence 
                    doc["{}_{:02d}".format(id,sent_index)] = sent_list[i] #Make each sentence in para have 1 line in dataframe and re-index them (_01, _02,...)
                    sent_index += 1
        else:
            doc["{}".format(id)] = None
    df_split = pd.DataFrame.from_dict(doc, orient='index', columns=["sent"])
    return df_split

if __name__ == '__main__':
    file_para=sys.argv[1]
    file_sent=sys.argv[2]
    # file_sent_raw=sys.argv[3]
    df = pd.read_csv(file_para,delimiter = ",", header = None, names = ["para"])
    df_split = split_para(df) #Apply function 
    # df_split["sent_norm"] = df_split["sent"].apply(norm_sentence)
    df_split.to_csv(file_sent, header=False, index = True)    
