import re
import requests
import pandas as pd
import sys
import re
import os
import pandas as pd
from nltk.tokenize import sent_tokenize
from sentence_splitter import SentenceSplitter
from nltk import word_tokenize
from utils import *
from number import *

def norm_sentence(line):
    line = str(line)
    line = line.strip()
    input_line = line
    #    line = normalize_email(line)
    line = tokenize(line)
    line_inp, line_out = normalize_09unit(line, line)
    line_inp, line_out = normalize_decimal(line_inp, line_out)
    line_inp, line_out = normalize_dottedwords(line_inp, line_out)
    line_inp, line_out = normalize_datetime(line_inp, line_out)
    # line_inp, line_out = norm_punct(line_inp, line_out)
    line_out = ' '.join([i for i in re.split(r'(\d+)', line_out) if i])
#     line_inp, line_out = norm_abbre(line_out, line_out, abbre_dict)
    line_inp, line_out = norm_tag_verbatim(line_inp, line_out)
    line_inp, line_out = normalize_decimal(line_inp, line_out)
#     line_inp, line_out = norm_foreign_words(line_inp, line_out, trans_dict=trans_dict)
    line_inp = line_inp.replace('_', ' ')
    line_out = line_out.replace('_', ' ')
    line_inp, line_out = normalize_AZ09(line_inp, line_out)
    line_inp, line_out = norm_tag_measure(line_inp, line_out)
#     line_inp, line_out = norm_tag_fraction(line_inp, line_out)
    line_inp, line_out = normalize_date_range(line_inp, line_out)
    line_inp, line_out = normalize_date(line_inp, line_out)
#     line_inp, line_out = normalize_time(line_inp, line_out)
    line_inp, line_out = normalize_phone_number(line_inp, line_out)
    line_inp, line_out = norm_digit(line_inp, line_out)
    line_inp, line_out = norm_tag_roman_num(line_inp, line_out)
    line_inp, line_out = normalize_number_range(line_inp, line_out)
#     line_inp, line_out = normalize_sport_score(line_inp, line_out)
    line_inp, line_out = normalize_number(line_inp, line_out)
    line_inp, line_out = normalize_letters(line_inp, line_out)
    line_out = remove_tag(line_out)
    return line_out


if __name__ == '__main__':
    file_sent=sys.argv[1]
    file_sent_norm = file_sent[:-4] + "_final.csv"
    df = pd.read_csv(file_sent,delimiter = ",", header =None, names = ["id","sent"])
    df["norm"] = df["sent"].apply(norm_sentence)
    df.to_csv(file_sent_norm, header=True, index = False)    

