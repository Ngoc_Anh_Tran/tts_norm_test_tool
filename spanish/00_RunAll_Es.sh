#!/bin/bash
# =========Norm text to spoken form========

working_folder=$(pwd)

echo -e "\n ==========  STEP 1: PRE-PROCESSING "
collection=ES20
file_para=${working_folder}/data/${collection}_para.csv
file_sent=${working_folder}/data/${collection}.csv


echo -e "\n ========== 1.1 Split paragraph (optional) "
if [ -f "$file_sent" ]; then
    echo "$file_sent exists => delete....."
    rm $file_sent
fi
python3 ${working_folder}/test/Es_split.py ${file_para} ${file_sent}

echo -e "\n ========== 1.2 Norm text ========"
python3 ${working_folder}/test/Es_norm.py ${file_sent}
