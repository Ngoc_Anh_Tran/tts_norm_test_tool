#!/bin/bash
# =========Norm text to spoken form========

working_folder=/home/anhttn18/Norm_TTS/tts_norm_test_tool/german

echo -e "\n ==========  STEP 1: PRE-PROCESSING "
collection=ge_16
file_para_raw=${working_folder}/data/${collection}_PARA.csv
file_sent_id=${working_folder}/data/${collection}_final.csv
# file_sent_raw=${working_folder}/data/fr_4c_sent_raw.txt
# file_sent_clean=${working_folder}/data/fr_4c_raw.txt
echo -e "\n ========== 1.1 Split paragraph (optional) "
python3 ${working_folder}/norm/Ge_norm.py ${file_para_raw} ${file_sent_id}

# echo -e "\n ========== 1.2 Clean text (txt file of sentences without ID)========"
# python3 ${working_folder}/norm/Fr_clean_text.py ${file_sent_raw} ${file_sent_clean}


# echo -e "\n=========== STEP 2: NORMALIZATION==========="