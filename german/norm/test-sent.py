import requests
import pandas as pd
import sys
import re
import os
import pandas as pd
from nltk.tokenize import sent_tokenize
from sentence_splitter import SentenceSplitter
from nltk import word_tokenize
import german_transliterate
from german_transliterate.core import GermanTransliterate
def clean_text(s):
    s=str(s)
#Thay thế kí tự có dấu và dấu tiếng Việt thành kí tự tương ứng trong tiếng Anh
    s = re.sub(r'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
    s = re.sub(r'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
    s = re.sub(r'[èéẹẻẽêềếệểễ]', 'e', s)
    s = re.sub(r'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
    s = re.sub(r'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
    s = re.sub(r'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
    s = re.sub(r'[ìíịỉĩ]', 'i', s)
    s = re.sub(r'[ÌÍỊỈĨ]', 'I', s)
    s = re.sub(r'[ùúụủũưừứựửữ]', 'u', s)
    s = re.sub(r'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
    s = re.sub(r'[ỳýỵỷỹ]', 'y', s)
    s = re.sub(r'[ỲÝỴỶỸ]', 'Y', s)
    s = re.sub(r'[Đ]', 'D', s)
    s = re.sub(r'[đ]', 'd', s)

#Remove some characters
    s = re.sub("\n", " ", s)    
    # text = re.sub(r'[A,a]\/[C,c]', 'AC', text)
    s = re.sub(r'[\«\»\–\“\”\‘\„\{\}\[\]\(\)\"\;]'," ", s) #Các dấu này norm bị lỗi phông hoặc không cần thiết
    s = re.sub("  ", " ", s)
    
    return s

def norm_punct(input_str, output_str):
    """
    Normalize punctuations: separate words and puctuations.
    Note that @, %, etc   will be changed, i.e. 'abc@gmail.com' --> 'abc @ gmail.com'
    """
    input_str = ' ' + input_str + ' '
    output_str = ' ' + output_str + ' '

    tokens = word_tokenize(input_str)
    input_str = " ".join(tokens)

    tokens = word_tokenize(output_str)
    output_str = " ".join(tokens)

    return ' '.join(input_str.split()), ' '.join(input_str.split())
def norm(line):
    ######### norm parameters
    ops = {'acronym_phoneme', 'accent_peculiarity', 'amount_money', 'date', 'timestamp',
       'weekday', 'month', 'time_of_day', 'ordinal', 'special', 'math_symbol', 'spoken_symbol'}
    line_inp, line_norm = norm_punct(line, line)
        # line_norm = GermanTransliterate(replace={';': ',', ':': ' '}, sep_abbreviation=' -- ').transliterate(line)
    line_norm = GermanTransliterate(transliterate_ops=list(ops-{'spoken_symbol', 'acronym_phoneme'}), sep_abbreviation=' ', make_lowercase=True).transliterate(line_norm)

    return line_norm
text = "Auch wenn viele Spitzen aus Bund und Ländern vor einer überstürzten Öffnung warnen – nicht zuletzt Angela Merkel selbst –, ganz ohne Perspektive für die Zeit nach dem harten Lockdown wird das Treffen kaum vorübergehen können."

print(norm(clean_text (text))) 