Tool: Package GermanTransliterate (See: "german_transliterate" folder)

How: Run 0_RunAll_Ge.sh only after edit the path of input file and output file

    Input: CSV file. 2 columns (para_id and raw para). Save in /german/data
    Ouput: CSV file. 3 columns (sentence id, raw sentence, normed sentence). Save in /german/data


Process: All steps in Ge_norm.py
    1. Pre process: Remove Vietnamese accent, unnecessary punctuation marks, ... 
    2. Split para into sentences
    3. Norm by function GermanTransliterate (you can change the config in core.py)
            Eg: GermanTransliterate(transliterate_ops=list(ops-{'spoken_symbol', 'acronym_phoneme'}), sep_abbreviation=' ', make_lowercase=True).transliterate(line_norm)

    4. Post process: Clean again to remove all unnecessary punctuation marks 

