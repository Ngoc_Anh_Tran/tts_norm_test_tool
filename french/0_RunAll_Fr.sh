#!/bin/bash
# =========Norm text to spoken form========

working_folder=/home/anhttn18/Norm_TTS/tts_norm_test_tool/french
irisa_folder=/home/anhttn18/Norm_TTS/tts_norm_fr_en_irisa
LANGUAGE="fr"

# echo -e "\n ==========  STEP 1: PRE-PROCESSING "
# file_para_raw=${working_folder}/data/fr_7b_PARA.csv
# file_sent_id=${working_folder}/data/fr_7b.csv
# echo -e "\n ========== 1.1 Split paragraph (optional) "
# python3 ${working_folder}/norm/Fr_split_para.py ${file_para_raw} ${file_sent_id}


echo -e "\n=========== STEP 2: NORMALIZATION==========="
file_sent_clean=${working_folder}/data/all_withoutID.txt
echo -e "\n ==========Tokenize========"
perl ${irisa_folder}/bin/$LANGUAGE/basic-tokenizer.pl ${file_sent_clean} > ${file_sent_clean}.tokenized
echo -e "\n ==========Start generic normalisation========"
perl ${irisa_folder}/bin/$LANGUAGE/start-generic-normalisation.pl ${file_sent_clean}.tokenized > ${file_sent_clean}.step1
echo -e "\n ==========End generic normalisation=========="
perl ${irisa_folder}/bin/$LANGUAGE/end-generic-normalisation.pl ${file_sent_clean}.step1 > ${file_sent_clean}.step2
echo -e "\n ==========Specific normalisation============="
perl ${irisa_folder}/bin/$LANGUAGE/specific-normalisation.pl ${irisa_folder}/cfg/asr_FR.cfg ${file_sent_clean}.step2 > ${file_sent_clean}.result
echo -e "\n ==========Remove unnecessary files======"
rm -f ${file_sent_clean}.tokenized
rm -f ${file_sent_clean}.step1
rm -f ${file_sent_clean}.step2


# # echo -e "\n=========== STEP 3: POST PROCESSING==========="
# sh ${working_folder}/norm/Fr_post_process.sh ${working_folder}/data/${file_sent_clean}.result

echo "\n===========Done ^.^ ============"




#echo -e "\n ==========Step 3 :TTS========"
# perl bin/$LANGUAGE/specific-normalisation.pl cfg/tts.cfg ${input_text_file_split}.step2 > ${input_text_file_split}.tts
# input_folder=../Fr_demo
# original_file=valid.raw
# # original_file=test.raw
#input_text_folder=/home/anhttn18/Workspace/tts_norm_fr_en_irisa/examples
# input_text_folder
#mkdir -p ${input_text_folder}
########## delete line longer than 200 characters ########33
# sed -r  "/.{200,}/d" ${input_folder}/${original_file} > ${input_text_folder}/${original_file}.shortline



######### Norm to spoken form ###########333
# ################################################3
# input_text_file=${input_text_folder}/${original_file}.shortline
# echo -e "\n ==========Split files: ${input_text_file}  ========="

# a=(`wc -l ${input_text_file}`)
# lines=`echo $(($a/8)) | bc -l`
# split -l $lines -d  ${input_text_file} ${input_text_file}.split

# echo -e "\n ==========Norm process : ${input_text_file}  ========="

# input_text_file_split=$1

# ################### CLEAN text ###############
# echo -e "\n ==========Step 4 : Clean text ========"
# out_folder=/media/khoamd/Hdd2TB_02/01_VinSTT_Data/01_Filito/02_French/fr_general_text/out_text_folder
# mkdir -p ${out_folder}
# clean_text=${out_folder}/file00.cleaned

# ########## delete empty line ########33
# sed '/^[[:space:]]*$/d' ${input_text_file_split}.step2 > ${clean_text}


########## concat files ########33
# cat
########## delete sentence with . ########33
# sed -i '/\./d' 

################ Add  sentence ID
# folder=/media/khoamd/Hdd2TB_02/01_VinSTT_Data/01_Filito/02_French/fr_general_text
# input_text=${folder}/input_text_folder/valid.raw.shortline.all.asr.cleaned.removeDot
# output_text=${input_text}.addSentID
# sentence_code=fr03_generaltext_

# python3 1_add_senID.py ${input_text} ${output_text} ${sentence_code}
