import pandas as pd
from nltk.tokenize import sent_tokenize
from sentence_splitter import SentenceSplitter
import sys


def clean_text(text):
    text = text.replace("\n", " ")
    text = text.replace("(", " ( ")
    text = text.replace(")", " ) ")
    text = text.replace(" - ", " ")
    text = text.replace("\"", " ")
    text = text.replace(":", " ")
    # text = text.replace(" . ", " ")
    text = text.replace("«", " ")
    text = text.replace("»", " ")
#     text = text.replace(".", " . ") # tach cac cau viet lien nhau ra. vi du: an apple.an orange --> an apple . an orang
#ko nên dùng lệnh trên vì sẽ biến các số 4.900 thành 4 . 900 và 900 sẽ bị split thành câu mới ==" 
    text = text.replace("  ", " ")
#     line = line.replace("?", " ")
#     line = line.replace("!", " ")
#     text = text.replace("\"","")
    return text
def split_para(dataframe):
    doc = {}
    df = dataframe.copy() #tránh việc các lệnh bên dưới làm thay đổi dataframe gốc 
    splitter = SentenceSplitter(language='fr')
    for id, row in df.iterrows():
        print(row[0])
        row[0] = clean_text(row[0])
        print(row[0])

        sent_list = splitter.split(text=row[0])
        print(row[0])
        sent_num = len(sent_list)
        if  sent_num == 1:
            doc["{}".format(id)] = row[0]  #lấy original input ở cột thứ 2
        elif sent_num > 1:
            for sent_index in range(sent_num):
               doc["{}_{:02d}".format(id,sent_index+1)] =  sent_list[sent_index]
        else:
            doc["{}".format(id)] = None
    df_split = pd.DataFrame.from_dict(doc, orient='index', columns=["sent"])
    return df_split

if __name__ == "__main__":
    file_para_raw="/home/anhttn18/Workspace/tts_norm_test_tool/french/data/test.csv"
    file_sent_raw_id="/home/anhttn18/Workspace/tts_norm_test_tool/french/data/test_split.csv"
    df = pd.read_csv(file_para_raw,delimiter = ",", header =None, names = ["para"])
    df_split = split_para(df)
    print(df_split)