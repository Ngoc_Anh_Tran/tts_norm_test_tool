import pandas as pd
from nltk.tokenize import sent_tokenize
from sentence_splitter import SentenceSplitter
import sys
import re
import nltk
from nltk.tokenize import sent_tokenize


def clean_text(s):
    s=str(s)
    #Remove some characters
    s = re.sub("\n", " ", s)
    s = re.sub(" - ", "-", s) #Tránh TH tách Vietnam - China thành Vietnam, xuống dòng China
    s = re.sub("([a-zA-Z])(\.)([A-Z])",r"\1. \3", s) # Tách abc.Xyz thành abc . xyz    
    # text = re.sub(r'[A,a]\/[C,c]', 'AC', text) 
    s = re.sub(r'[\«\»\–\“\”\‘\„\{\}\[\]\(\)\"\;]'," ", s) #Các dấu này norm bị lỗi phông hoặc không cần thiết
    s = re.sub("  ", " ", s)
    return s

    

def split_para(dataframe):
    doc = {}
    df = dataframe.copy() #tránh việc các lệnh bên dưới làm thay đổi dataframe gốc 
    splitter = SentenceSplitter(language='fr')
    for id, row in df.iterrows():
        row[0] = clean_text(row[0])
        sent_list = splitter.split(row[0])
        sent_num = len(sent_list)
        if  sent_num == 1:
            doc["{}".format(id)] = row[0]  #lấy original input ở cột thứ 2
        elif sent_num > 1:
            for sent_index in range(sent_num):
               doc["{}_{:02d}".format(id,sent_index+1)] =  sent_list[sent_index]
        else:
            doc["{}".format(id)] = None
    df_split = pd.DataFrame.from_dict(doc, orient='index', columns=["sent"])
    return df_split

if __name__ == "__main__":
    file_para_raw=sys.argv[1]
    file_sent_id=sys.argv[2]
    df = pd.read_csv(file_para_raw,delimiter = ",", header =None, names = ["para"])
    df_split = split_para(df) #Apply function 
    df_split.to_csv(file_sent_id, header=None, index = True)