import requests

text = "HBO; phần rất nhỏ này trên thực tế cũng đã được dàn trải tới tận 170 quốc gia và vùng lãnh thổ trên toàn thế giới (một phép ẩn dụ nho nhỏ cho việc toàn thế giới đều đã bị cuốn theo vòng xoáy của Ngai Thép)"
api_norm_v1='http://10.124.68.83:5041/internal_api/v1/tts_norm'
api_norm_v2='http://10.124.68.83:5042/internal_api/v1/tts_norm'
# api_norm_server

def norm_tts(sent):
    try:
        data = {"paragraph": sent} #English: "language":"english"
        result = requests.post(api_norm_v2, json = data)  
        normed_list = result.json().get("normed_sentences")
        output = " ".join(normed_list)
        return output
    except:
        return None
print(norm_tts(text))


# url_groundtruth='http://127.0.0.1:11993/norm'
# def norm_tts_groundtruth(sent):
#     data = {"sample": sent}
#     result = requests.post(url_groundtruth, json = data)
#     js = result.json()
#     output = js.get('result')
#     output = output.replace("độ c", "độ xê")
#     output = output.replace("độ f", "độ ép")
#     return output

# print(norm_tts_groundtruth(text ))