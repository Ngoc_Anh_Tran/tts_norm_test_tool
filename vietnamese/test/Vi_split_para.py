'''

===== Step 1: Split (Optional) ====
INPUT REQUIREMENT : CSV file with Column 1 (ID), Column 2 (para), NO header 

'''
import requests
import pandas as pd
import sys
import re
import nltk
# nltk.download('punkt')
from nltk.tokenize import sent_tokenize

# #Define necessary paths
url_split = sys.argv[1]
file_para = sys.argv[2]
file_input_split = sys.argv[3]

#Splitting function
def para_split(para):
    para = str(para)
    para = re.sub("\n", " . ", para)
    para = re.sub("|", " . ", para)

    data = {"paragraph": para}
    result = requests.post(url_split, json=data)
    sent_list = result.json().get("unnormed_sentences")
#     normed_list = result.json().get("normed_sentences")
    return sent_list

if __name__ == '__main__':
    #Read input file
    df_para = pd.read_csv(file_para, delimiter = ",", names = ["para"]) #delimiter = "\t" if input is .txt
    print(df_para.head())

    doc = {} #Create a dictionary {"sent id": sent} 
    punc_list = ["!",".","?"] #List of punctuation

    for id, row in df_para.iterrows():
        old_text=row["para"]
        new_text=old_text.replace("\n", "  . ")
        new_text=new_text.replace("|", "  . ")
        row.para=new_text
        # sent_list = para_split(row.para)
        sent_list = sent_tokenize(row.para)
        sent_index = 1
        for i in range(len(sent_list)):
            if sent_list[i] not in punc_list:
                doc["{}_{:02d}".format(id,sent_index)] = sent_list[i]
                sent_index += 1

    #Convert the above dictionary to a dataframe and save to csv file
    data = pd.DataFrame.from_dict(doc, orient='index')
    data.reset_index(inplace=True)
    print(data.head(5))
    data.to_csv(file_input_split+".csv", index=False, header=False)
