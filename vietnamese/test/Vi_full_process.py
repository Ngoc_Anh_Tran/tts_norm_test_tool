{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "vital-research",
   "metadata": {},
   "source": [
    "# Step 1: Split (Optional)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spanish-sector",
   "metadata": {},
   "source": [
    "#### Input requirement:\n",
    "    - CSV file: Column 1 (ID), Column 2 (para), NO header"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "domestic-capacity",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "para    Bạn có thể nói các câu lệnh như [ \"mở cốp xe\",...\n",
      "Name: Guide-Car Control, dtype: object\n",
      "para    Bạn có thể nói các câu lệnh như [ \"Bật đài FM\"...\n",
      "Name: Guide-Radio, dtype: object\n",
      "para    Bạn có thể nói các câu lệnh như [ \"Bật đài FM\"...\n",
      "Name: Guide-Radio, dtype: object\n"
     ]
    }
   ],
   "source": [
    "#pip install requests\n",
    "#pip install pandas\n",
    "#pip install numpy\n",
    "\n",
    "import requests\n",
    "import pandas as pd\n",
    "\n",
    "#Define necessary paths\n",
    "url_split = 'http://10.124.68.92:11993/norm'\n",
    "file_para = \"../data/demo.csv\"\n",
    "file_input_split = \"../data/demo-split.csv\"\n",
    "\n",
    "#Splitting function\n",
    "def para_split(para):\n",
    "    data = {\"paragraph\": para}\n",
    "    result = requests.post(url_split, json=data)\n",
    "    sent_list = result.json().get(\"unnormed_sentences\")\n",
    "#     normed_list = result.json().get(\"normed_sentences\")\n",
    "    return sent_list\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    #Read input file\n",
    "    df_para = pd.read_csv(file_para, delimiter = \",\", names = [\"para\"]) #delimiter = \"\\t\" if input is .txt\n",
    "\n",
    "    doc = {} #Create a dictionary {\"sent id\": sent} \n",
    "    punc_list = [\"!\",\".\",\"?\"] #List of punctuation\n",
    "\n",
    "    for id, row in df_para.iterrows():\n",
    "        print(row)\n",
    "        sent_list = para_split(row.para)\n",
    "        sent_index = 1\n",
    "        for i in range(len(sent_list)):\n",
    "            if sent_list[i] not in punc_list:\n",
    "                doc[\"{}_{:02d}\".format(id,sent_index)] = sent_list[i]\n",
    "                sent_index += 1\n",
    "\n",
    "    #Convert the above dictionary to a dataframe and save to csv file\n",
    "    data = pd.DataFrame.from_dict(doc, orient='index')\n",
    "    data.reset_index(inplace=True)\n",
    "    data.to_csv(file_input_split, index=False, header=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "velvet-jaguar",
   "metadata": {},
   "source": [
    "## Step 2: Norm & Compare with Groundtruth"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "emerging-investing",
   "metadata": {},
   "source": [
    "#### Input requirement: \n",
    "- CSV \n",
    "- Column 1 (ID) & Column 2 (Text) \n",
    "- NO HEADER"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "damaged-pasta",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Final result: \n",
      " True     0.978082\n",
      "False    0.021918\n",
      "Name: result, dtype: float64\n",
      "Time processing: \n",
      " 11.46431303024292 s\n"
     ]
    }
   ],
   "source": [
    "#pip install requests\n",
    "#pip install pandas\n",
    "#pip install numpy\n",
    "\n",
    "import requests\n",
    "import pandas as pd\n",
    "import time \n",
    "\n",
    "start = time.time()\n",
    "\n",
    "#Define necessary paths\n",
    "url = 'http://10.124.68.92:11993/norm'\n",
    "url_groundtruth = 'http://127.0.0.1:11993/norm'\n",
    "file_input = \"../data/vinfast-testcase-en.csv\"\n",
    "file_output = \"../data/vinfast-testcase-en-result.csv\"\n",
    "\n",
    "#Test TTS normalization tool\n",
    "def norm_tts(sent):\n",
    "    data = {\"paragraph\": sent}\n",
    "    result = requests.post(url, json=data)\n",
    "    unnormed_list = result.json().get(\"unnormed_sentences\")\n",
    "    normed_list = result.json().get(\"normed_sentences\")\n",
    "    return \" . \".join(normed_list) + \" .\"\n",
    "\n",
    "#Get groundtruth data by tool in repo: tts_norm_service \n",
    "def norm_tts_groundtruth(sent):\n",
    "    data = {\"sample\": sent}\n",
    "    result = requests.post(url_groundtruth, json = data)\n",
    "    text_output = result.json()\n",
    "    return text_output.get('result')\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    #Read txt input file\n",
    "    df = pd.read_csv(file_input, names = [\"id\",\"input\"])\n",
    "    df[\"norm\"] = df[\"input\"].apply(norm_tts)\n",
    "    df[\"groundtruth\"] = df[\"input\"].apply(norm_tts_groundtruth)\n",
    "    df[\"result\"] = df[\"groundtruth\"] == df[\"norm\"]\n",
    "    df.to_csv(file_output, index=False)\n",
    "    \n",
    "    # Show the final statistic\n",
    "    print(\"Final result: \\n\",  df[\"result\"].value_counts()/df.shape[0])\n",
    "    print(\"Time processing: \\n {} s\".format(time.time()-start))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}