'''

===== Step 2: Norm & Compare with Groundtruth ====

INPUT REQUIREMENT : 
- CSV 
- Column 1 (ID) & Column 2 (Text) 
- NO HEADER

'''

import requests
import pandas as pd
import time 
import sys
import re
import os

start = time.time()

#Define necessary paths
url_norm = sys.argv[1]
url_groundtruth = sys.argv[2]
file_input_split = sys.argv[3]
file_output_norm = sys.argv[4]

#Clean text
def clean(text):
    text = str(text)
    # text = re.sub(r'[^\w\s]','',text)
    text = text.replace("  "," ")
    text = text.strip().lower()
    return text

def clean_keep_punc(text):
    text = str(text)
    text = text.replace("!"," ! ")
    text = text.replace("?"," ? ")
    text = text.replace("."," . ")
    text = text.replace("_"," ")

    text = re.sub(r"\)\]\}\_\"\'\"\-\:\“\”\‘\’\„"," ",text) #Dau bo qua
    text = re.sub(r"\;\(", " , ",text)  #Chuyen thanh dau phay

    text = text.replace("  "," ")
    text = text.replace("  "," ")
    text = text.strip().lower()
    return text
#Test TTS normalization tool

def norm_tts(sent):
    try:
        data = {"paragraph": sent} #English: "language":"english"
        result = requests.post(url_norm, json = data)  
        normed_list = result.json().get("normed_sentences")
        output = " ".join(normed_list)
        return output
    except:
        return None

#Get groundtruth data by tool in repo: tts_norm_service 
def norm_tts_groundtruth(sent):
    data = {"sample": sent}
    result = requests.post(url_groundtruth, json = data)
    js = result.json()
    output = js.get('result')
    #Compare thì bật các dòng dưới lên ====
    # output = output.replace("độ c", "độ xê")
    # output = output.replace("độ f", "độ ép")
    # output = output.replace("hvac", "hát vê a xê")
    # # Bật bên trên lên nếu compare
    return output.strip()

if __name__ == '__main__':
    #Read csv input file
    df = pd.read_csv(file_input_split+".csv", names = ["id","input"])
    print(df.head())
    df["norm"] = df["input"].apply(norm_tts)
    df["clean"] = df["norm"].apply(clean)
    # df["groundtruth"] = df["input"].apply(norm_tts_groundtruth)
    print(df.head())
    
    # ========Bật lên nếu compare============== 
    # df["norm_clean"] = df["norm"].apply(clean)
    # df["groundtruth_clean"] = df["groundtruth"].apply(clean) #Delete punctuation
    # df["result"] = df["norm_clean"] == df["groundtruth_clean"]
    # #Show the final statistic
    # print("Final result: \n",  df["result"].value_counts()/df.shape[0])
    # print("Time processing: \n {} s".format(time.time()-start))
    # ========Bật lên nếu compare===============
    # if os.path.exists(file_output_split):
    #     os.remove(file_output_split)
    # else:
    #     print("Can not delete the file as it doesn't exists")
    df.to_csv(file_output_norm+".csv", header = True, index=False) #cột ID đã thành 1 cột bình thường rồi, giữ index sẽ thành 0,1,2...
    
    