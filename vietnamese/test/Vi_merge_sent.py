'''

===== Step 3: Merrge sentences (Optional) ====
INPUT REQUIREMENT : CSV file with Column 1 (Sent_ID), Column 2 (Sentence checked with audio), NO header 

'''
import pandas as pd
import sys

# #Define necessary file path
file_script_sent = sys.argv[1]
file_script_para = sys.argv[2]

#Merge function


if __name__ == '__main__':
    #Read input file
    df_sent = pd.read_csv(file_script_sent, delimiter = ",", names = ["sent_id","sent"]) #delimiter = "\t" if input is .txt

    new_doc = {} #Create a dictionary {"para id": para} 
    para_id = "shortPara_Sci_150para_00001"
    k = len(para_id)
    para = []

    for id, row in df_sent.iterrows():        
        if row["sent_id"][:k] == para_id:
            para.append(row["sent"])
        else: 
            new_doc[para_id]=" ".join(para).strip()
            para = []
            para_id = row["sent_id"][:k]
            para.append(row["sent"])
    new_doc[para_id]=" ".join(para).strip() # Dòng cuối cùng :D 
    #Convert the above dictionary to a dataframe and save to csv file
    data = pd.DataFrame.from_dict(new_doc, orient='index')
    data.reset_index(inplace=True)
    data.to_csv(file_script_para, index=False, header=False)
