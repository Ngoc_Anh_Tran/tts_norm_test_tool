import requests


def norm_sent(sent,api_norm):
    try:
        data = {"paragraph": sent} #norm tiếng Việt
        # data = {"paragraph": sent, "lang":"en"} #norm tiếng Anh
        result = requests.post(api_norm, json = data)  
        wr_words = result.json().get("wr_words")
        sp_words = result.json().get("sp_words")
        normed_list = result.json().get("normed_sents")
        output = " ".join(normed_list)
        print(f'\nInput: {sent} \n')
        print(f'Tagging: {wr_words} \n')
        print(f'Expansion: {sp_words} \n')
        print(f'Final: {output.lower()} \n')
        return output
    except:
        print("False")
        return None

def norm_doc(path, api_norm):
    normed_list = []
    with open(path,mode="r", encoding='utf8') as f:
        for line in f:
            id = line.split("\t")[0]
            sent = line.split("\t")[1]
            normed_sent = norm_sent(sent, api_norm)
            normed_list.append(normed_sent)
    with open(path + "_norm", mode="w", encoding = "utf8") as f:
        for ele in normed_list:
            f.write(f'{id}\t{ele}\n')
    print(f"Done normalizing document: {path}")

if __name__ == "__main__":
    # api_norm_v1='http://10.124.68.83:5041/internal_api/v1/tts_norm' #(may 83)
    # api_norm_v2='http://10.124.68.83:5042/internal_api/v1/tts_norm' #(may 83)
    api_server_v2="http://10.254.148.20:5042/internal_api/v2/tts_norm" #(may 20)

    ### Norm 1 sentence:

    sent = "CH số 3"
    norm_sent(sent,api_server_v2) 

    # ## Norm 1 document:

    # # doc_path = "/mnt/4T/khoamd/02_code/eval_norm/issue/sms_update_vocab"
    # doc_path = "/mnt/4T/khoamd/01_database/01_Vietnamese/N_F01_KhanhLinh/00_Test/transcript_test"
    # norm_doc(doc_path,api_server_v2) 

    ## Norm many documents:
    # path_list = ["data/test1", "data/test2"]
    # for doc_path in path_list:
    #     norm_doc(doc_path.api_server_v2)
