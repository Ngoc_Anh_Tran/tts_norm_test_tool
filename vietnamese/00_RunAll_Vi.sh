# !/bin/bash

working_folder=/home/anhttn18/Workspace/tts_norm_test_tool/vietnamese
url_norm_v1='http://10.124.68.83:5041/internal_api/v1/tts_norm'
url_norm_v2='http://10.124.68.83:5042/internal_api/v1/tts_norm'
url_groundtruth='http://127.0.0.1:11993/norm'

# declare -a list_collection=("TTS_Playback_VF_VA_Media" "TTS_Playback_VF_VA_Phone_Call" "TTS_Playback_VF_VA_Cliamate_Control" "TTS_Playback_VF_VA_Radio" "TTS_Playback_VF_VA_Audio")
# declare -a list_collection=("MinhLuan_VN02, MinhLuan_VN10, MinhLuan_VN11, MinhLuan_VN12, MinhLuan_VN20")
declare -a list_collection=("KhanhLinh_VN03_update")
# declare -a list_collection=("KhanhLinh_VN25" "KhanhLinh_VN26" "KhanhLinh_VN30" "KhanhLinh_VN31")

# cd $working_folder
for collection in ${list_collection[@]}; do
    file_para="data/${collection}_PARA.csv"
    file_input_split="data/${collection}"
    ver="final"
    file_output_norm=${file_input_split}_$ver

    # echo -e "\n ==========Step 1-Split para (optional)========"
    # if [ -f $file_input_split ]; then
    #     echo "$file_input_split exists => deletes..."
    #     rm $file_input_split
    # fi

    # python3 test/Vi_split_para.py ${url_norm_v1} ${file_para} ${file_input_split} #tách câu bằng API 

    echo -e "\n ========== Step 2: Norm & Compare with Groundtruth ========"
    if [ -f $file_output_norm ]; then
        echo "$file_output_norm exists => deletes..."
        rm $file_output_norm
    fi

    python3 test/Vi_norm_compare.py ${url_norm_v1} ${url_groundtruth} ${file_input_split} ${file_output_norm}

    # # echo -e "\n ========== Step 3: Merge sentences into paragraph ========"
    # # file_script_sent="data/vi-sci-script.csv"
    # # file_script_para="data/vi-sci-script_FINAL.csv"
    # # python3 test/Vi_merge_sent.py ${file_script_sent} ${file_script_para} 

    echo -e "Done! :D "
done

    
