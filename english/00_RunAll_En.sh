#!/bin/bash

working_folder=/home/anhttn18/Norm_TTS/tts_norm_test_tool/english
irisa_folder=/home/anhttn18/Norm_TTS/tts_norm_fr_en_irisa
LANGUAGE="en"
collection="en_16"

echo -e "\n ==========  STEP 1: PRE-PROCESSING "
echo -e "\n ========== 1.1 Split paragraph (optional) "
echo -e "\n ========== 1.2 Clean text and split (txt file of sentences without ID)========"
file_para_raw=${working_folder}/data/${collection}_PARA.csv
file_sent_id=${working_folder}/data/$collection.csv
file_sent_only=${working_folder}/data/$collection.txt
python3 ${working_folder}/norm/En_split_para.py ${file_para_raw} ${file_sent_id} $file_sent_only

echo -e "\n=========== STEP 2: NORMALIZATION==========="
echo -e "\n ==========Tokenize========"
perl ${irisa_folder}/bin/$LANGUAGE/basic-tokenizer.pl ${file_sent_only} > ${file_sent_only}.tokenized
echo -e "\n ==========Start generic normalisation========"
perl ${irisa_folder}/bin/$LANGUAGE/start-generic-normalisation.pl ${file_sent_only}.tokenized > ${file_sent_only}.step1
echo -e "\n ==========End generic normalisation=========="
perl ${irisa_folder}/bin/$LANGUAGE/end-generic-normalisation.pl ${file_sent_only}.step1 > ${file_sent_only}.step2
echo -e "\n ==========Specific normalisation============="
perl ${irisa_folder}/bin/$LANGUAGE/specific-normalisation.pl ${irisa_folder}/cfg/asr_EN.cfg ${file_sent_only}.step2 > ${file_sent_only}.result
echo -e "\n ==========Remove unnecessary files======"
# rm -f ${file_sent_only}
rm -f ${file_sent_only}.tokenized
rm -f ${file_sent_only}.step1
rm -f ${file_sent_only}.step2
tr '[:upper:]' '[:lower:]' < ${file_sent_only}.result > ${file_sent_only}.final #Lower text (CASE_LOW in cfg - does not work)
rm -f ${file_sent_only}.result #Delete result file, only keep file input and file final


# echo -e "\n=========== STEP 3: POST PROCESSING==========="
# # sh ${working_folder}/norm/En_post_process.sh ${file_sent_id}.final
# echo "\n===========Done ^.^ ============"

## REFERENCE 

#echo -e "\n ==========Step 3 :TTS========"
# perl bin/$LANGUAGE/specific-normalisation.pl cfg/tts.cfg ${input_text_file_split}.step2 > ${input_text_file_split}.tts

#mkdir -p ${input_text_folder}
########## delete line longer than 200 characters ########33
# sed -r  "/.{200,}/d" ${input_folder}/${original_file} > ${input_text_folder}/${original_file}.shortline



######### Norm to spoken form ###########333
# ################################################3
# input_text_file=${input_text_folder}/${original_file}.shortline
# echo -e "\n ==========Split files: ${input_text_file}  ========="

# a=(`wc -l ${input_text_file}`)
# lines=`echo $(($a/8)) | bc -l`
# split -l $lines -d  ${input_text_file} ${input_text_file}.split

# echo -e "\n ==========Norm process : ${input_text_file}  ========="

# input_text_file_split=$1

# ################### CLEAN text ###############
# echo -e "\n ==========Step 4 : Clean text ========"
# out_folder=/media/khoamd/Hdd2TB_02/01_VinSTT_Data/01_Filito/02_French/fr_general_text/out_text_folder
# mkdir -p ${out_folder}
# clean_text=${out_folder}/file00.cleaned

# ########## delete empty line ########33
# sed '/^[[:space:]]*$/d' ${input_text_file_split}.step2 > ${clean_text}


########## concat files ########33
# cat
########## delete sentence with . ########33
# sed -i '/\./d' 

################ Add  sentence ID
# folder=/media/khoamd/Hdd2TB_02/01_VinSTT_Data/01_Filito/02_French/fr_general_text
# input_text=${folder}/input_text_folder/valid.raw.shortline.all.asr.cleaned.removeDot
# output_text=${input_text}.addSentID
# sentence_code=fr03_generaltext_

# python3 1_add_senID.py ${input_text} ${output_text} ${sentence_code}
