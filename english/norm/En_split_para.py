import requests
import pandas as pd
import sys
import re
import os
from sentence_splitter import SentenceSplitter


def remove_bf_split(s):
    s = re.sub("\n", " ", s)
    s = re.sub(r" - ", "-", s)
    return s

def clean_text(s):
    #Thay thế kí tự có dấu và dấu tiếng Việt thành kí tự tương ứng trong tiếng Anh
    s = re.sub(r'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
    s = re.sub(r'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
    s = re.sub(r'[èéẹẻẽêềếệểễ]', 'e', s)
    s = re.sub(r'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
    s = re.sub(r'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
    s = re.sub(r'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
    s = re.sub(r'[ìíịỉĩ]', 'i', s)
    s = re.sub(r'[ÌÍỊỈĨ]', 'I', s)
    s = re.sub(r'[ùúụủũưừứựửữ]', 'u', s)
    s = re.sub(r'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
    s = re.sub(r'[ỳýỵỷỹ]', 'y', s)
    s = re.sub(r'[ỲÝỴỶỸ]', 'Y', s)
    s = re.sub(r'[Đ]', 'D', s)
    s = re.sub(r'[đ]', 'd', s)
    
    #Remove some characters
    s = re.sub(r'[‘’]',"\'", s)
    s = re.sub(r'[«»\–\—\“”‘’„{}\[\]\(\)\"\;]'," ", s) #Các dấu này norm bị lỗi phông hoặc không cần thiết
    s = re.sub(r"%","percent", s)
    
    # s = re.sub(r'[A,a]\/[C,c]', 'AC', text)#cho VINFAST Response
    s = re.sub(r'USD', ' USD ', s)
    s = re.sub(r'VND', ' VND ', s)
    s = re.sub(r"US\$","US $", s)
    s = re.sub(r"\$\s*1 ","one dollar ", s)
    s = re.sub(r'(\$)\s*(\d+([,\.]\d+)?)', r'\2 dollars', s)

    s = re.sub("  ", " ", s)
    return s

def split_para(dataframe):
    doc = {}
    df = dataframe.copy() #tránh việc các lệnh bên dưới làm thay đổi dataframe gốc 
    splitter = SentenceSplitter(language='en')
    for id, row in df.iterrows():
        row[0] = remove_bf_split(row[0])
        sent_list = splitter.split(text=row[0])
        sent_num = len(sent_list)
        if  sent_num == 1:
            doc["{}".format(id)] = row[0]  #lấy original input ở cột thứ 2
        elif sent_num > 1:
            for sent_index in range(sent_num):
               doc["{}_{:02d}".format(id,sent_index+1)] =  sent_list[sent_index]
        else:
            doc["{}".format(id)] = None
    df_split = pd.DataFrame.from_dict(doc, orient='index', columns=["sent"])
    df_split["sent_clean"] = df_split["sent"].apply(clean_text)
    return df_split

if __name__ == "__main__":
    file_para_raw=sys.argv[1]
    file_sent_id=sys.argv[2]
    file_sent_only=sys.argv[3]

    df = pd.read_csv(file_para_raw,delimiter = ",", header =None, names = ["para"])
    print(df.head())
    df_split = split_para(df) #Apply function 
    df_split.to_csv(file_sent_id, header=True, index = True)

    with open(file_sent_only, 'w') as f:
        for text in df_split['sent_clean'].tolist():
            f.write(text + '\n')