import re
import pandas as pd

text = "CALL me 0915256678"
a = re.search("^0\d{9,10}",text)
print(a)

text = "Ngỡ lÀ không Ăn " 
#Thay thế kí tự có dấu và dấu tiếng Việt thành kí tự tương ứng trong tiếng Anh
def nonAccentVietnamese(s):
    s = re.sub(r'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
    s = re.sub(r'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
    s = re.sub(r'[èéẹẻẽêềếệểễ]', 'e', s)
    s = re.sub(r'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
    s = re.sub(r'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
    s = re.sub(r'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
    s = re.sub(r'[ìíịỉĩ]', 'i', s)
    s = re.sub(r'[ÌÍỊỈĨ]', 'I', s)
    s = re.sub(r'[ùúụủũưừứựửữ]', 'u', s)
    s = re.sub(r'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
    s = re.sub(r'[ỳýỵỷỹ]', 'y', s)
    s = re.sub(r'[ỲÝỴỶỸ]', 'Y', s)
    s = re.sub(r'[Đ]', 'D', s)
    s = re.sub(r'[đ]', 'd', s)
    return s
print(nonAccentVietnamese(text))
def replace_text(text):
    text = re.sub(r" - ", "-", text)
    text = re.sub(r'[A,a]\/[C,c]', 'AC', text)
    text = re.sub(r"%","percent", text)
    return text

df_origin = pd.read_csv('/home/anhttn18/Workspace/tts_norm_test_tool/english/data/en_05_raw.txt', delimiter="\t", names=["sent"])
df = df_origin.copy()
df["sent"] = df["sent"].apply(replace_text)
print(df["sent"])

df["sent"] = df["sent"].apply(nonAccentVietnamese)
print(df["sent"])

print(df["sent"])