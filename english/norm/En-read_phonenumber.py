import re

text = "Call me 0915324975" 

def expand_NDIG(w):
    """Expand digit sequences to a series of words."""
    try:
        numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        num_words = ['oh', 'one', 'two', 'three', 'four', 'five',
                     'six', 'seven', 'eight', 'nine']
        str2 = ''
        for n in w:
            if n.isdigit():
                str2 += num_words[numbers.index(n)]
                str2 += " "
            elif n == '.':
                str2 += 'dot'
                str2 += ' '
            else:
                str2 += ''
        return str2[:-1]
    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        return w

def read_phonenum(sent):
    sent_list = sent.split()
    for i in range (len(sent_list)):
        if sent_list[i].isnumeric():
            sent_list[i] = expand_NDIG(sent_list[i])
    new_sent = " ".join(sent_list)
    return new_sent
# 




